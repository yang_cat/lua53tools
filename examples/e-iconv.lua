local iconv = require "iconv"

local u2g = iconv.new("GBK", "UTF-8")
local g2u = iconv.new("UTF-8", "GBK")

local s = "你好"
print("ORIG: " .. s)

local s2 = u2g:iconv(s)
print("GBK: " .. s2)

local s3 = g2u:iconv(s2)
print("UTF-8: " .. s3)


