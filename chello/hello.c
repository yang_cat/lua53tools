#include "lua.h"
#include "lauxlib.h"

const char *message = "hello world!";

int say(lua_State *L) {
    lua_pushstring(L, message);
    return 1;
}

static const struct luaL_Reg cfunctions [] = {
    {"say", say},
    {NULL, NULL}
};

int luaopen_hello(lua_State *L) {
    luaL_newlibtable(L, cfunctions);
    luaL_setfuncs(L, cfunctions, 0);
    return 1;
}
