print("-------------------md5------------------------")
local md5=require("md5")
for k, v in pairs(md5) do
	print (k, v)
end
print("-------------------lpeg-----------------------")
local lpeg=require("lpeg")
for k, v in pairs(lpeg) do
	print (k, v)
end
print("-------------------lua-utf8-------------------")
local utf8=require("lua-utf8")
for k, v in pairs(utf8) do
	print (k, v)
end
print("-------------------socket.http----------------")
local http=require("socket.http")
for k, v in pairs(http) do
	print (k, v)
end
print("-------------------ssl.https------------------")
local https = require 'ssl.https'
for k, v in pairs(https) do
	print (k, v)
end
--local a, b, c, d = https.request {url="https://www.google.com", protocol="any", redirect=false}
--print("Status:", c)
print("-------------------lsqlite3-------------------")
local sqlite3=require("lsqlite3")
for k, v in pairs(sqlite3) do
	print (k, v)
end
print("-------------------rex_pcre-------------------")
local re=require("rex_pcre")
for k, v in pairs(re) do
	print (k, v)
end
print("-------------------luasql.mysql---------------")
local mysql=require("luasql.mysql")
for k, v in pairs(mysql) do
	print (k, v)
end
--local env  = mysql.mysql()
--local conn = env:connect('test','root','0000')
--print(env, conn)
print("-------------------lfs------------------------")
local lfs = require("lfs")
for k, v in pairs(lfs) do
	print(k,v)
end
print("-------------------pl-------------------------")
local pl = require("pl.utils")
for k, v in pairs(pl) do
	print(k, v)
end
print("-------------------ml-------------------------")
local ml = require("ml")
for k, v in pairs(ml) do
	print(k, v)
end
print("-------------------luazen---------------------")
local lz = require("luazen")
for k, v in pairs(lz) do
	print(k, v)
end
print("-------------------cjson----------------------")
local cj = require("cjson")
for k, v in pairs(cj) do
	print(k, v)
end
